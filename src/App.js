import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Welcome from './components/welcome';
import Main from './router/main';
import {Link} from  'react-router-dom'


class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
        <Link to="/">Home</Link>
        <Link to="/add">Add News</Link>
          Meows
        </header>
        <Main/>
      </div>
    );
  }
}

export default App;
