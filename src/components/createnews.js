import React, {Component} from 'react';
import axios from 'axios';
export default  class CreateNews extends Component {
  constructor(props){
    super(props)
    this.state={
      title:"",
      author:"",
      body:""
    }
    this.createNews=this.createNews.bind(this);
    this.changeTitle=this.changeTitle.bind(this);
    this.changeAuthor=this.changeAuthor.bind(this);
    this.changeBody=this.changeBody.bind(this);
    this.redirectNews=this.redirectNews.bind(this);
  }

  componentDidMount(){

  }

 redirectNews(){
   this.props.history.push('/')
 }

  createNews(){
     axios.post('http://localhost:3001/news/add', this.state)
    .then(res=>{
      this.setState({  title:"",
        author:"",
        body:""})
        console.log(res.data)
        this.props.history.push('/');
    }).catch(err=>{
      alert('error');
      console.log(err);
    })
  }
  changeTitle(event){
    this.setState({title:event.target.value})
  }
  changeAuthor(event){
    this.setState({author:event.target.value})
  }
  changeBody(event){
    this.setState({body:event.target.value})
  }
  render(){
    return(
    <div>
      <p><input value={this.state.title} onChange={this.changeTitle} type="text" placeholder="title"/></p>
      <p><input value={this.state.author} onChange={this.changeAuthor} type="text" placeholder="author"/></p>
      <p><textarea placeholder="body" onChange={this.changeBody}>{this.state.body}</textarea></p>
      <p><button onClick={this.createNews}>save</button></p>
    </div>
    );
  }
}
