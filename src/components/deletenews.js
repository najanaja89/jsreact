import React, {
  Component
} from 'react';
import axios from 'axios';
import {
  Link
} from 'react-router-dom';
export default class DeleteNews extends Component {
  constructor(props) {
    super(props)
    this.state = {
      id: props.match.params.id,
      title: "",
      author: "",
      body: ""
    }
    this.deleteNews = this.deleteNews.bind(this);
    this.loadOne = this.loadOne.bind(this);
  }

  componentDidMount() {
    this.loadOne();
  }

  loadOne() {
    axios.get('http://localhost:3001/news/edit/' + this.state.id)
      .then(res => {
        let data = res.data;
        this.setState({
          title: data.title,
          author: data.author,
          body: data.body
        })
      }).catch(err => {
        alert('error');
        console.log(err);
      })
  }

  deleteNews() {
    axios.post('http://localhost:3001/news/delete/' + this.state.id, this.state)
      .then(res => {
        this.setState({
          title: "",
          author: "",
          body: ""
        })
        this.props.history.push('/')
        console.log(res.data)
      }).catch(err => {
        alert('error');
        console.log(err);
      })
  }

  render() {
    return ( <
      div >
      <
      p > < input value = {
        this.state.title
      }
      type = "text"
      placeholder = "title" / > < /p> <
      p > < input value = {
        this.state.author
      }
      type = "text"
      placeholder = "author" / > < /p> <
      p > < textarea placeholder = "body"
      value = {
        this.state.body
      } > < /textarea></p >
      <
      p > < button onClick = {
        this.deleteNews
      } > confirm delete < /button></p >
      <
      /div>
    );
  }
}