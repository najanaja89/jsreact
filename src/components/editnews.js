import React, {
  Component
} from 'react';
import axios from 'axios';
import {
  Link
} from 'react-router-dom';
export default class EditNews extends Component {
  constructor(props) {
    super(props)
    this.state = {
      id: props.match.params.id,
      title: "",
      author: "",
      body: ""
    }
    this.editNews = this.editNews.bind(this);
    this.changeTitle = this.changeTitle.bind(this);
    this.changeAuthor = this.changeAuthor.bind(this);
    this.changeBody = this.changeBody.bind(this);
    this.loadOne = this.loadOne.bind(this);
  }

  componentDidMount() {
    this.loadOne();
  }

  loadOne() {
    axios.get('http://localhost:3001/news/edit/' + this.state.id)
      .then(res => {
        let data = res.data;
        this.setState({
          title: data.title,
          author: data.author,
          body: data.body
        })
      }).catch(err => {
        alert('error');
        console.log(err);
      })
  }

  editNews() {
    axios.post('http://localhost:3001/news/edit/' + this.state.id, this.state)
      .then(res => {
        this.setState({
          title: "",
          author: "",
          body: ""
        })
        console.log(res.data)
      }).catch(err => {
        alert('error');
        console.log(err);
      })
  }
  changeTitle(event) {
    this.setState({
      title: event.target.value
    })
  }
  changeAuthor(event) {
    this.setState({
      author: event.target.value
    })
  }
  changeBody(event) {
    this.setState({
      body: event.target.value
    })
  }
  render() {
    return ( <
      div >
      <
      p > < input value = {
        this.state.title
      }
      onChange = {
        this.changeTitle
      }
      type = "text"
      placeholder = "title" / > < /p> <
      p > < input value = {
        this.state.author
      }
      onChange = {
        this.changeAuthor
      }
      type = "text"
      placeholder = "author" / > < /p> <
      p > < textarea placeholder = "body"
      onChange = {
        this.changeBody
      }
      value = {
        this.state.body
      } > < /textarea></p >
      <
      p > < button onClick = {
        this.editNews
      } > save < /button></p >
      <
      /div>
    );
  }
}
