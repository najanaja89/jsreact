import React, {
  Component
} from 'react';
import moment from 'moment';
import axios from 'axios';
import {
  Link
} from 'react-router-dom';
export default class NewsList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      count: 0,
      news: [{
          title: 'Title1',
          author: 'Author1',
          date: new Date()
        },
        {
          title: 'Title2',
          author: 'Author2',
          date: new Date()
        },
        {
          title: 'Title3',
          author: 'Author3',
          date: new Date()
        },
      ]
    }
    this.incCount = this.incCount.bind(this);
    this.loadNews = this.loadNews.bind(this);
  }

  componentDidMount() {
    this.loadNews()
  }

  incCount() {
    let count = this.state.count + 1;
    this.setState({
      count: count
    })
  }
  loadNews() {
    axios.get('http://localhost:3001/news')
      .then(res => {
        this.setState({
          news: res.data
        })
      }).catch(err => {
        alert('error');
        console.log(err);
      })
  }
  render() {
    return ( <
      div > {
        this.state.news.map(item => {
          return ( <
            div >
            <
            h1 > {
              item.title
            } < /h1> <
            p > {
              item.author
            } < /p> <
            p > {
              moment(item.date).format('DD.MM.YY HH:mm')
            } < /p> <
            p > {
              item.body
            } < /p> <
            p > < Link to = {
              `edit/${item._id}`
            } > Edit < /Link></p >
            <
            p > < Link to = {
              `delete/${item._id}`
            } > Delete < /Link></p >
            <
            /div>
          )
        })
      } <
      p > count: {
        this.state.count
      } < /p> <
      p > < button onClick = {
        this.incCount
      } > Add count < /button></p >
      <
      p > < button onClick = {
        this.loadNews
      } > Load News < /button></p >
      <
      /div>
    );
  }
}