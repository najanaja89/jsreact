import React, {
  Component
} from 'react';
import {
  Route
} from 'react-router-dom';
import NewsList from '../components/news';
import Welcome from '../components/welcome';
import CreateNews from '../components/createnews';
import EditNews from '../components/editnews';
import DeleteNews from '../components/deletenews';
export default class Main extends Component {

  render() {
    return ( <
      div >
      <
      Route exact path = '/'
      component = {
        NewsList
      }
      /> <
      Route path = '/add'
      component = {
        CreateNews
      }
      /> <
      Route path = '/edit/:id'
      component = {
        EditNews
      }
      /> <
      Route path = '/delete/:id'
      component = {
        DeleteNews
      }
      /> < /
      div > )
  }
}